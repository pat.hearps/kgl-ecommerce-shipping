.PHONY: local setup install jupyter fresh freeze update download_data

local: setup fresh jupyter pre-commit freeze download_data

setup:
	mkdir -p data/raw data/processed data/analysis data/results data/interim
	mkdir -p notebooks/scratch
	mkdir -p figures

# to create conda env from scratch
fresh: ~/miniconda*/envs/kgl_ship/bin/python3.8

~/miniconda*/envs/kgl_ship/bin/python: # proxy for if whole environment exists
	conda env create --file start_environment.yaml

# mainly for observing exact changes to environment via git
freeze:
	conda list --name kgl_ship --export > full_environment.yaml

# main command to run to update existing environment with new requirements
update:
	conda env update --name kgl_ship --file start_environment.yaml --prune
	make freeze

# note: you will probably need nodejs specifically version 12 for jupyter-plotly to work
# instructions at https://github.com/nodesource/distributions/blob/master/README.md
jupyter:
	jupyter labextension install jupyterlab-plotly@4.14.3

pre-commit:
	pre-commit install

download_data:
	aws s3 sync s3://analytics-data-repository/kaggle/ecommerce-shipping/data/raw/ data/raw

data: data/processed/engineered.csv


data/processed/cleaned.csv: data/raw/train.csv
	python -m src.data.clean

data/processed/engineered.csv: data/processed/cleaned.csv
	python -m src.data.feature_engineering
