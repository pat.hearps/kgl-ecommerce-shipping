# Kaggle Ecommerce Shipping

Internal Biarri Analytics team project to practice data science on Kaggle competition "E-Commerce Shipping Data":
https://www.kaggle.com/prachi13/customer-analytics

# Team guidelines

## Notebooks

### When to commit

Please start working on jupyter notebooks from the folder notebooks/scratch/, which is
gitignored. Only move it up to notebooks/ and commit if and when you have got it to a
point where it:

1. concisely shows only relevant and useful analysis
2. can run through cells sequentially, without errors
3. you have 'restarted kernel and cleared all outputs' to avoid committing contents of
   notebook output cells e.g. rows of actual pandas dataframes, plotting images etc which
   unnecessarily take up repo space

### Creating and naming

- Recommend creating new notebooks by duplicating `/notebooks/base_notebook.ipynb`, which has
  an initial cell that will move the working directory up to the repo root regardless of
  where the file is.
- Use a naming convention with a major and minor number, followed by 3-letter initials,
  followed by a useful short description, separated with underscores, e.g.<br>
  `1.01_pjh_initial_eda.ipynb` <br>
  Numbering system is a guideline only to help understand order of work, try to keep
  major numbers related to separate major problem solving steps e.g.
  - 1 = EDA
  - 2 = feature engineering, data processing
  - 3 = modelling
  - 4 = results analysis

# Setup

Run `make local` to do all initial setup steps. This includes the conda commands outlined below.

### Conda

We will use conda for creating a virtual environment. If you don't already have it, [install Miniconda](https://docs.conda.io/en/latest/miniconda.html) - much simpler than full Anaconda.

3 Make commands for conda:

- the first time you create the environment, run `make fresh`. Make sure you set up your
  IDE to use the environment that gets created
- run `make update` to update the existing environment with new contents of start_environment.yaml
  Any additions to requirements are to be made in start_environment.yaml - the intention
  is to only specify the packages we definitely want and let conda figure out
  dependencies.
- run `make freeze` to dump the exact specifications of the new environment to full_environment.yaml
  the intention of this file is to have an exact record of what the environment should contain,
  and allow us to see the changes via git tracking
