from pathlib import Path


class DIR:
    PROJECT = Path(__file__).parent.parent.resolve()
    DATA = PROJECT / "data"
    RAW = DATA / "raw"
    ANALYSIS = DATA / "analysis"
    PROC = DATA / "processed"
    INTERIM = DATA / "interim"
    RESULTS = DATA / "results"
