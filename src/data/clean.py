import pandas as pd
from src.constants import DIR


def main() -> pd.DataFrame:
    df = pd.read_csv(DIR.RAW/'train.csv', index_col='ID')
    # Improve column names
    df = df.rename(columns={
        "Warehouse_block": "warehouse",
        "Mode_of_Shipment": "shipping",
        "Customer_care_calls": "customer_calls",
        "Customer_rating": "rating",
        "Cost_of_the_Product": "price",
        "Prior_purchases": "prior_purchases",
        "Product_importance": "priority",
        "Gender": "gender",
        "Discount_offered": "discount",
        "Weight_in_gms": "weight",
        "Reached.on.Time_Y.N": "timely_arrival",
    })

    df.to_csv(DIR.PROC/'cleaned.csv', index=False)

    return df


if __name__ == '__main__':
    ecommerce_df = main()
