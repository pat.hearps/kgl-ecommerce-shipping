import pandas as pd
from src.constants import DIR

def main() -> None:

    cleaned_df = pd.read_csv(DIR.PROC/'cleaned.csv')

    cleaned_df['discounted_price'] = cleaned_df.price * (1 - cleaned_df.discount / 100)

    cleaned_df.to_csv(DIR.PROC/'engineered.csv', index=False)


if __name__ == '__main__':
    main()
